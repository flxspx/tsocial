$(document).ready(function () {
    $('#'+idt+' thead tr')
        .clone(true)
        .addClass('filters')
        .appendTo('#'+idt+' thead');

    var t = $('#'+idt).DataTable({

        dom: 'Bfrtip',
        buttons: [
            'excel','print'
        ],
        
                                     
        scrollY: 350,
        scrollX: true,
        scrollColapse: true,

        paging: false,
        responsive: true,
        /*fixedColumns: {
            left: 1,
            right: 1
        },*/

        orderCellsTop: true,
        fixedHeader: true,
        
        
        initComplete: function(){
            var api = this.api();

            api
                .columns()
                .eq(0)
                .each(function (colIdx){
                    var cell = $('.filters th').eq(
                        $(api.column(colIdx).header()).index()
                    );
                    var title = $(cell).text();
                    if ($(api.column(colIdx).header()).index() >= 1) {
                        $(cell).html('<input type="text" placeholder="'+ title + '" />');
                    }

                    $(
                        'input',
                        $('.filters th').eq($(api.column(colIdx).header()).index())
                    )
                    .off('keyup change')
                    .on('change', function (e){
                        $(this).attr('title',$(this).val());
                        var regexr = '({search})';
                        var cursorPosition = this.selectionStart;

                        api
                            .column(colIdx)
                            .search(
                                this.value != ''
                                    ? regexr.replace('{search}', '(((' + this.value + ')))')
                                    : '',
                                this.value != '',
                                this.value != ''
                            )
                            .draw();
                    })
                    .on('keyup', function(e){
                        var cursorPosition = this.selectionStart;
                        e.stopPropagation();
                        $(this).trigger('change');
                        $(this)
                            .focus()[0]
                            .setSelectionRange(cursorPosition, cursorPosition);
                    });
                });
        },
        
        columnDefs: [
            {
                targets: 0,
                width: '15%',
                searchable: false,
                orderable: false,
                
            },
        ],
        
        order: [[1, 'asc']],
        
        /*columnDefs: [
            { responsivePriority: 1, targets: 0 },
            { responsivePriority: 2, targets: -1 }
        ],*/
        
        

        "language": {
            "emptyTable":   "Sin Registros Encontrados",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 a 0 de 0 Entradas",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "search": "Buscar:",
            "zeroRecords":  "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },

                    
    
    
    }); //FIN DE DATATABLES

    t.on('order.dt search.dt', function () {
        let i = 1;
        t.cells(null, 0, { search: 'applied', order: 'applied' }).every(function (cell) {
            this.data(i++);
        });
    }).draw();
                      
});//FIN DE DOCUMENT