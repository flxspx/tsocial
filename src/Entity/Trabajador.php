<?php

namespace App\Entity;

use App\Repository\TrabajadorRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TrabajadorRepository::class)]
class Trabajador
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 50)]
    private $nombre;

    #[ORM\Column(type: 'string', length: 23)]
    private $apellidoP;

    

    #[ORM\Column(type: 'string', length: 23)]
    private $apellidoM;

    #[ORM\Column(type: 'string', length: 255)]
    private $adscripcion;

    #[ORM\Column(type: 'string', length: 14)]
    private $rfc;

    #[ORM\Column(type: 'string', length: 18)]
    private $curp;

    #[ORM\ManyToOne(targetEntity: Turno::class, inversedBy: 'trabajadores')]
    #[ORM\JoinColumn(nullable: false)]
    private $turno;

    #[ORM\ManyToOne(targetEntity: Ttrabajador::class, inversedBy: 'trabajadores')]
    #[ORM\JoinColumn(nullable: false)]
    private $trabajadorTipo;

    #[ORM\Column(type: 'string', length: 255)]
    private $codigo;

    #[ORM\Column(type: 'string', length: 255)]
    private $descripcionCodigo;

    #[ORM\ManyToOne(targetEntity: Perfil::class, inversedBy: 'trabajadores')]
    #[ORM\JoinColumn(nullable: false)]
    private $perfil;

    #[ORM\Column(type: 'string', length: 255)]
    private $funcion;

    #[ORM\Column(type: 'string', length: 255)]
    private $cargo;

    #[ORM\Column(type: 'datetime')]
    private $fechaIngreso;

    #[ORM\Column(type: 'string', length: 255)]
    private $correo;

    #[ORM\Column(type: 'string', length: 10)]
    private $telefono;

    #[ORM\Column(type: 'string', length: 47)]
    private $cedula;

    #[ORM\ManyToOne(targetEntity: Unidades::class, inversedBy: 'trabajadores')]
    #[ORM\JoinColumn(nullable: false, name: 'unidad_id', referencedColumnName: 'UnidadId')]
    private $unidad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellidoP(): ?string
    {
        return $this->apellidoP;
    }

    public function setApellidoP(string $apellidoP): self
    {
        $this->apellidoP = $apellidoP;

        return $this;
    }

    public function getApellidoM(): ?string
    {
        return $this->apellidoM;
    }

    public function setApellidoM(string $apellidoM): self
    {
        $this->apellidoM = $apellidoM;

        return $this;
    }

    public function getAdscripcion(): ?string
    {
        return $this->adscripcion;
    }

    public function setAdscripcion(string $adscripcion): self
    {
        $this->adscripcion = $adscripcion;

        return $this;
    }

    public function getRfc(): ?string
    {
        return $this->rfc;
    }

    public function setRfc(string $rfc): self
    {
        $this->rfc = $rfc;

        return $this;
    }

    public function getCurp(): ?string
    {
        return $this->curp;
    }

    public function setCurp(string $curp): self
    {
        $this->curp = $curp;

        return $this;
    }

    public function getTurno(): ?Turno
    {
        return $this->turno;
    }

    public function setTurno(?Turno $turno): self
    {
        $this->turno = $turno;

        return $this;
    }

    public function getTrabajadorTipo(): ?Ttrabajador
    {
        return $this->trabajadorTipo;
    }

    public function setTrabajadorTipo(?Ttrabajador $trabajadorTipo): self
    {
        $this->trabajadorTipo = $trabajadorTipo;

        return $this;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getDescripcionCodigo(): ?string
    {
        return $this->descripcionCodigo;
    }

    public function setDescripcionCodigo(string $descripcionCodigo): self
    {
        $this->descripcionCodigo = $descripcionCodigo;

        return $this;
    }

    public function getPerfil(): ?Perfil
    {
        return $this->perfil;
    }

    public function setPerfil(?Perfil $perfil): self
    {
        $this->perfil = $perfil;

        return $this;
    }

    public function getFuncion(): ?string
    {
        return $this->funcion;
    }

    public function setFuncion(string $funcion): self
    {
        $this->funcion = $funcion;

        return $this;
    }

    public function getCargo(): ?string
    {
        return $this->cargo;
    }

    public function setCargo(string $cargo): self
    {
        $this->cargo = $cargo;

        return $this;
    }

    public function getFechaIngreso(): ?\DateTimeInterface
    {
        return $this->fechaIngreso;
    }

    public function setFechaIngreso(\DateTimeInterface $fechaIngreso): self
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    public function setCorreo(string $correo): self
    {
        $this->correo = $correo;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getCedula(): ?string
    {
        return $this->cedula;
    }

    public function setCedula(string $cedula): self
    {
        $this->cedula = $cedula;

        return $this;
    }

    public function getUnidad(): ?Unidades
    {
        return $this->unidad;
    }

    public function setUnidad(?Unidades $unidad): self
    {
        $this->unidad = $unidad;

        return $this;
    }

    public function __toString() {
        return $this->nombre;
    }

}
