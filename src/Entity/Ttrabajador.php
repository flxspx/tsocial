<?php

namespace App\Entity;

use App\Repository\TtrabajadorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TtrabajadorRepository::class)]
class Ttrabajador
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 23)]
    private $tipoTrabajador;

    #[ORM\OneToMany(mappedBy: 'trabajadorTipo', targetEntity: Trabajador::class)]
    private $trabajadores;

    public function __construct()
    {
        $this->trabajadores = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipoTrabajador(): ?string
    {
        return $this->tipoTrabajador;
    }

    public function setTipoTrabajador(string $tipoTrabajador): self
    {
        $this->tipoTrabajador = $tipoTrabajador;

        return $this;
    }

    /**
     * @return Collection<int, Trabajador>
     */
    public function getTrabajadores(): Collection
    {
        return $this->trabajadores;
    }

    public function addTrabajadore(Trabajador $trabajadore): self
    {
        if (!$this->trabajadores->contains($trabajadore)) {
            $this->trabajadores[] = $trabajadore;
            $trabajadore->setTrabajadorTipo($this);
        }

        return $this;
    }

    public function removeTrabajadore(Trabajador $trabajadore): self
    {
        if ($this->trabajadores->removeElement($trabajadore)) {
            // set the owning side to null (unless already changed)
            if ($trabajadore->getTrabajadorTipo() === $this) {
                $trabajadore->setTrabajadorTipo(null);
            }
        }

        return $this;
    }
    public function __toString() {
        return $this->tipoTrabajador;
    }
}
