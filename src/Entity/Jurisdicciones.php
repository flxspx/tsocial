<?php

namespace App\Entity;

use App\Repository\JurisdiccionesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JurisdiccionesRepository::class)]
#[ORM\Table(name: 'Jurisdicciones')]

class Jurisdicciones
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', name: 'JurisdiccionId')]
    private $id;

    #[ORM\Column(type: 'string', length: 11, name: 'Clues')]
    private $clues;

    #[ORM\Column(type: 'string', length: 40, name: 'Nombre', nullable: true)]
    private $nombre;

    #[ORM\OneToMany(mappedBy: 'jurisdiccion', targetEntity: Municipios::class)]
    private $municipios;

    #[ORM\OneToMany(mappedBy: 'jurisdiccion', targetEntity: Unidades::class)]
    private $unidades;

    public function __construct()
    {
        $this->municipios = new ArrayCollection();
        $this->unidades = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getclues(): ?string
    {
        return $this->clues;
    }

    public function setclues(string $clues): self
    {
        $this->clues = $clues;

        return $this;
    }

    public function getnombre(): ?string
    {
        return $this->nombre;
    }

    public function setnombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection<int, Municipios>
     */
    public function getMunicipios(): Collection
    {
        return $this->municipios;
    }

    public function addMunicipio(Municipios $municipio): self
    {
        if (!$this->municipios->contains($municipio)) {
            $this->municipios[] = $municipio;
            $municipio->setjurisdiccion($this);
        }

        return $this;
    }

    public function removeMunicipio(Municipios $municipio): self
    {
        if ($this->municipios->removeElement($municipio)) {
            // set the owning side to null (unless already changed)
            if ($municipio->getjurisdiccion() === $this) {
                $municipio->setjurisdiccion(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Unidades>
     */
    public function getUnidades(): Collection
    {
        return $this->unidades;
    }

    public function addUnidade(Unidades $unidade): self
    {
        if (!$this->unidades->contains($unidade)) {
            $this->unidades[] = $unidade;
            $unidade->setjurisdiccion($this);
        }

        return $this;
    }

    public function removeUnidade(Unidades $unidade): self
    {
        if ($this->unidades->removeElement($unidade)) {
            // set the owning side to null (unless already changed)
            if ($unidade->getjurisdiccion() === $this) {
                $unidade->setjurisdiccion(null);
            }
        }

        return $this;
    }
    public function __toString() {
        return $this->nombre;
    }
}
