<?php

namespace App\Entity;

use App\Repository\MunicipiosRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MunicipiosRepository::class)]
#[ORM\Table(name: 'Municipios')]

class Municipios
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', name: 'MunicipioId')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Jurisdicciones::class, inversedBy: 'municipios')]
    #[ORM\JoinColumn(nullable: false, name: 'JurisdiccionId', referencedColumnName: 'JurisdiccionId')]
    private $jurisdiccion;

    #[ORM\Column(type: 'smallint', name: 'MunicipioPaisId', nullable: true)]
    private $municipioPais;

    #[ORM\Column(type: 'string', length: 60, name: 'NombreMunicipio', nullable: true)]
    private $nombreMunicipio;

    #[ORM\Column(type: 'string', length: 4, name: 'siglas', nullable: true)]
    private $siglas;

    #[ORM\OneToMany(mappedBy: 'municipio', targetEntity: Unidades::class)]
    private $unidades;

    public function __construct()
    {
        $this->unidades = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getjurisdiccion(): ?Jurisdicciones
    {
        return $this->jurisdiccion;
    }

    public function setjurisdiccion(?Jurisdicciones $jurisdiccion): self
    {
        $this->jurisdiccion = $jurisdiccion;

        return $this;
    }

    public function getmunicipioPais(): ?int
    {
        return $this->municipioPais;
    }

    public function setmunicipioPais(int $municipioPais): self
    {
        $this->municipioPais = $municipioPais;

        return $this;
    }

    public function getnombreMunicipio(): ?string
    {
        return $this->nombreMunicipio;
    }

    public function setnombreMunicipio(string $nombreMunicipio): self
    {
        $this->nombreMunicipio = $nombreMunicipio;

        return $this;
    }

    public function getsiglas(): ?string
    {
        return $this->siglas;
    }

    public function setsiglas(string $siglas): self
    {
        $this->siglas = $siglas;

        return $this;
    }

    /**
     * @return Collection<int, Unidades>
     */
    public function getunidades(): Collection
    {
        return $this->unidades;
    }

    public function addUnidade(Unidades $unidade): self
    {
        if (!$this->unidades->contains($unidade)) {
            $this->unidades[] = $unidade;
            $unidade->setmunicipio($this);
        }

        return $this;
    }

    public function removeUnidade(Unidades $unidade): self
    {
        if ($this->unidades->removeElement($unidade)) {
            // set the owning side to null (unless already changed)
            if ($unidade->getmunicipio() === $this) {
                $unidade->setmunicipio(null);
            }
        }

        return $this;
    }
    public function __toString() {
        return $this->nombreMunicipio;
    }
}
