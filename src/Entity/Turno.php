<?php

namespace App\Entity;



use App\Repository\TurnoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TurnoRepository::class)]
class Turno
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 23)]
    private $nombreTurno;

    #[ORM\OneToMany(mappedBy: 'turno', targetEntity: Trabajador::class)]
    private $trabajadores;

    public function __construct()
    {
        $this->trabajadores = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreTurno(): ?string
    {
        return $this->nombreTurno;
    }

    public function setNombreTurno(string $nombreTurno): self
    {
        $this->nombreTurno = $nombreTurno;

        return $this;
    }

    /**
     * @return Collection<int, Trabajador>
     */
    public function getTrabajadores(): Collection
    {
        return $this->trabajadores;
    }

    public function addTrabajadore(Trabajador $trabajadore): self
    {
        if (!$this->trabajadores->contains($trabajadore)) {
            $this->trabajadores[] = $trabajadore;
            $trabajadore->setTurno($this);
        }

        return $this;
    }

    public function removeTrabajadore(Trabajador $trabajadore): self
    {
        if ($this->trabajadores->removeElement($trabajadore)) {
            // set the owning side to null (unless already changed)
            if ($trabajadore->getTurno() === $this) {
                $trabajadore->setTurno(null);
            }
        }

        return $this;
    }
    public function __toString() {
        return $this->nombreTurno;
    }
}
