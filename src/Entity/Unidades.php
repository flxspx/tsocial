<?php

namespace App\Entity;

use App\Repository\UnidadesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UnidadesRepository::class)]
#[ORM\Table(name: 'Unidades')]

class Unidades
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer', name: 'UnidadId')]
    private $id;

    #[ORM\Column(type: 'smallint', name: 'Acreditada')]
    private $acreditada;

    #[ORM\Column(type: 'decimal', precision: 10, scale: 2, name: 'Altura', nullable: true)]
    private $altura;

    #[ORM\Column(type: 'text', name: 'Calle', nullable: true)]
    private $calle;

    #[ORM\Column(type: 'string', length: 11, name: 'Clues', nullable: true)]
    private $clues;

    #[ORM\Column(type: 'integer', name: 'CodigoPostal', nullable: true)]
    private $codigoPostal;

    #[ORM\Column(type: 'string', length: 60, name: 'CorreoUnidad', nullable: true)]
    private $correoUnidad;

    #[ORM\Column(type: 'integer', name: 'EmpleadoId', nullable: true)]
    private $empleado;

    #[ORM\Column(type: 'smallint', name: 'EstadoUnidad', nullable: true)]
    private $estadoUnidad;

    #[ORM\Column(type: 'datetime', name: 'FechaActualizacion', nullable: true)]
    private $fechaActualizacion;

    #[ORM\Column(type: 'string', length: 20, name: 'FechaInicioConstruccion', nullable: true)]
    private $fechaInicioConstruccion;

    #[ORM\Column(type: 'string', length: 20, name: 'FechaInicioOperacion', nullable: true)]
    private $fechaInicioOperacion;

    #[ORM\ManyToOne(targetEntity: Jurisdicciones::class, inversedBy: 'unidades')]
    #[ORM\JoinColumn(nullable: false, name: 'JurisdiccionId', referencedColumnName: 'JurisdiccionId')]
    private $jurisdiccion;

    #[ORM\Column(type: 'string', length: 20, name: 'Latitud', nullable: true)]
    private $latitud;

    #[ORM\Column(type: 'string', length: 20, name: 'Longitud', nullable: true)]
    private $longitud;

    #[ORM\Column(type: 'string', length: 6, name: 'MarcacionInterna', nullable: true)]
    private $marcacionInterna;

    #[ORM\ManyToOne(targetEntity: Municipios::class, inversedBy: 'unidades')]
    #[ORM\JoinColumn(nullable: false, name: 'MunicipioId', referencedColumnName: 'MunicipioId')]
    private $municipio;

    #[ORM\Column(type: 'string', length: 120, name: 'NombreUnidad')]
    private $nombreUnidad;

    #[ORM\Column(type: 'text', name: 'NombreUnidadCredencial')]
    private $nombreUnidadCredencial;

    #[ORM\Column(type: 'string', length: 45, name: 'Telefono1', nullable: true)]
    private $telefono1;

    #[ORM\Column(type: 'string', length: 45, name: 'Telefono2', nullable: true)]
    private $telefono2;

    #[ORM\Column(type: 'smallint', name: 'TipologiaUnidadId')]
    private $tipologiaUnidad;

    #[ORM\Column(type: 'smallint', name: 'TotalCamas')]
    private $totalCamas;

    #[ORM\Column(type: 'smallint', name: 'TotalConsultorios')]
    private $totalConsultorios;

    #[ORM\Column(type: 'smallint', name: 'LocalidadId')]
    private $localidad;

    #[ORM\OneToMany(mappedBy: 'unidad', targetEntity: Trabajador::class)]
    private $trabajadores;

    public function __construct()
    {
        $this->trabajadores = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAcreditada(): ?int
    {
        return $this->acreditada;
    }

    public function setAcreditada(int $acreditada): self
    {
        $this->acreditada = $acreditada;

        return $this;
    }

    public function getAltura(): ?string
    {
        return $this->altura;
    }

    public function setAltura(?string $altura): self
    {
        $this->altura = $altura;

        return $this;
    }

    public function getCalle(): ?string
    {
        return $this->calle;
    }

    public function setCalle(?string $calle): self
    {
        $this->calle = $calle;

        return $this;
    }

    public function getClues(): ?string
    {
        return $this->clues;
    }

    public function setClues(?string $clues): self
    {
        $this->clues = $clues;

        return $this;
    }

    public function getCodigoPostal(): ?int
    {
        return $this->codigoPostal;
    }

    public function setCodigoPostal(?int $codigoPostal): self
    {
        $this->codigoPostal = $codigoPostal;

        return $this;
    }

    public function getCorreoUnidad(): ?string
    {
        return $this->correoUnidad;
    }

    public function setCorreoUnidad(?string $correoUnidad): self
    {
        $this->correoUnidad = $correoUnidad;

        return $this;
    }

    public function getEmpleado(): ?int
    {
        return $this->empleado;
    }

    public function setEmpleado(?int $empleado): self
    {
        $this->empleado = $empleado;

        return $this;
    }

    public function getEstadoUnidad(): ?int
    {
        return $this->estadoUnidad;
    }

    public function setEstadoUnidad(?int $estadoUnidad): self
    {
        $this->estadoUnidad = $estadoUnidad;

        return $this;
    }

    public function getFechaActualizacion(): ?\DateTimeInterface
    {
        return $this->fechaActualizacion;
    }

    public function setFechaActualizacion(?\DateTimeInterface $fechaActualizacion): self
    {
        $this->fechaActualizacion = $fechaActualizacion;

        return $this;
    }

    public function getFechaInicioConstruccion(): ?string
    {
        return $this->fechaInicioConstruccion;
    }

    public function setFechaInicioConstruccion(?string $fechaInicioConstruccion): self
    {
        $this->fechaInicioConstruccion = $fechaInicioConstruccion;

        return $this;
    }

    public function getFechaInicioOperacion(): ?string
    {
        return $this->fechaInicioOperacion;
    }

    public function setFechaInicioOperacion(?string $fechaInicioOperacion): self
    {
        $this->fechaInicioOperacion = $fechaInicioOperacion;

        return $this;
    }

    public function getLatitud(): ?string
    {
        return $this->latitud;
    }

    public function setLatitud(?string $latitud): self
    {
        $this->latitud = $latitud;

        return $this;
    }

    public function getLongitud(): ?string
    {
        return $this->longitud;
    }

    public function setLongitud(?string $longitud): self
    {
        $this->longitud = $longitud;

        return $this;
    }

    public function getMarcacionInterna(): ?string
    {
        return $this->marcacionInterna;
    }

    public function setMarcacionInterna(?string $marcacionInterna): self
    {
        $this->marcacionInterna = $marcacionInterna;

        return $this;
    }

    public function getNombreUnidad(): ?string
    {
        return $this->nombreUnidad;
    }

    public function setNombreUnidad(string $nombreUnidad): self
    {
        $this->nombreUnidad = $nombreUnidad;

        return $this;
    }

    public function getNombreUnidadCredencial(): ?string
    {
        return $this->nombreUnidadCredencial;
    }

    public function setNombreUnidadCredencial(string $nombreUnidadCredencial): self
    {
        $this->nombreUnidadCredencial = $nombreUnidadCredencial;

        return $this;
    }

    public function getTelefono1(): ?string
    {
        return $this->telefono1;
    }

    public function setTelefono1(?string $telefono1): self
    {
        $this->telefono1 = $telefono1;

        return $this;
    }

    public function getTelefono2(): ?string
    {
        return $this->telefono2;
    }

    public function setTelefono2(?string $telefono2): self
    {
        $this->telefono2 = $telefono2;

        return $this;
    }

    public function getTipologiaUnidad(): ?int
    {
        return $this->tipologiaUnidad;
    }

    public function setTipologiaUnidad(int $tipologiaUnidad): self
    {
        $this->tipologiaUnidad = $tipologiaUnidad;

        return $this;
    }

    public function getTotalCamas(): ?int
    {
        return $this->totalCamas;
    }

    public function setTotalCamas(int $totalCamas): self
    {
        $this->totalCamas = $totalCamas;

        return $this;
    }

    public function getTotalConsultorios(): ?int
    {
        return $this->totalConsultorios;
    }

    public function setTotalConsultorios(int $totalConsultorios): self
    {
        $this->totalConsultorios = $totalConsultorios;

        return $this;
    }

    public function getLocalidad(): ?int
    {
        return $this->localidad;
    }

    public function setLocalidad(int $localidad): self
    {
        $this->localidad = $localidad;

        return $this;
    }

    public function getJurisdiccion(): ?Jurisdicciones
    {
        return $this->jurisdiccion;
    }

    public function setJurisdiccion(?Jurisdicciones $jurisdiccion): self
    {
        $this->jurisdiccion = $jurisdiccion;

        return $this;
    }

    public function getMunicipio(): ?Municipios
    {
        return $this->municipio;
    }

    public function setMunicipio(?Municipios $municipio): self
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * @return Collection<int, Trabajador>
     */
    public function getTrabajadores(): Collection
    {
        return $this->trabajadores;
    }

    public function addTrabajadore(Trabajador $trabajadore): self
    {
        if (!$this->trabajadores->contains($trabajadore)) {
            $this->trabajadores[] = $trabajadore;
            $trabajadore->setUnidad($this);
        }

        return $this;
    }

    public function removeTrabajadore(Trabajador $trabajadore): self
    {
        if ($this->trabajadores->removeElement($trabajadore)) {
            // set the owning side to null (unless already changed)
            if ($trabajadore->getUnidad() === $this) {
                $trabajadore->setUnidad(null);
            }
        }

        return $this;
    }
    public function __toString() {
        return $this->nombreUnidad;
    }
    
}
