<?php

namespace App\Form;

use App\Entity\Unidades;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UnidadesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('acreditada')
            ->add('altura')
            ->add('calle')
            ->add('clues')
            ->add('codigoPostal')
            ->add('correoUnidad')
            ->add('empleado')
            ->add('estadoUnidad')
            ->add('fechaActualizacion')
            ->add('fechaInicioConstruccion')
            ->add('fechaInicioOperacion')
            ->add('latitud')
            ->add('longitud')
            ->add('marcacionInterna')
            ->add('nombreUnidad')
            ->add('nombreUnidadCredencial')
            ->add('telefono1')
            ->add('telefono2')
            ->add('tipologiaUnidad')
            ->add('totalCamas')
            ->add('totalConsultorios')
            ->add('localidad')
            ->add('jurisdiccion')
            ->add('municipio')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Unidades::class,
        ]);
    }
}
