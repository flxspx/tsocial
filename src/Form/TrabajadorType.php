<?php

namespace App\Form;

use App\Entity\Trabajador;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class TrabajadorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombre')
            ->add('apellidoP', null, [
                'label'=>'Apellido Paterno'                
            ] )
            ->add('apellidoM', null, [
                'label'=>'Apellido Materno'
            ])
            ->add('unidad', null, [
                'label'=>'Unidad de Trabajo'
            ])
            ->add('adscripcion', null, [
                'label'=>'Adscripción'
            ])
            ->add('rfc', null, [
                'label'=>'RFC'
                ])
            ->add('curp', null, [
                'label'=>'CURP',
                'required'=>false,
                'empty_data'=>'XXXX999999XXXXXX99'
                ])
            ->add('codigo', null, [
                'label'=>'Código',
                'attr' => ['placeholder' => 'Ejem. M03025'],
            ])
            
            ->add('descripcionCodigo',  null, [
                'label'=>'Descripcion del Código',
                'attr' => ['placeholder' => 'Ejem. Técnico En Trabajado Social'],
            ])
            ->add('funcion', null, [
                'label'=>'Función',
                'attr' => ['placeholder' => 'Ejem.Tecnico en Trabajo Social'],
            ])
            ->add('cargo', null, [
                'label'=>'Cargo',
                'attr' => ['placeholder' => 'Jefe de Departamento'],
            ])
            ->add('fechaIngreso', DateType::class,[
                'label'=>'Fecha de Ingreso',
                'widget'=>'single_text',
                'required'=>true,
                'html5'=>false,
                'format'=>'dd/MM/yyyy',
            ])
            ->add('correo', null, [
                'label'=>'Correo Electronico',
                'attr' => ['placeholder' => 'Ejem. usuario@gmail.com'],
            ])
            ->add('telefono', null, [
                'label'=>'Num. Telefonico',
                'attr' => ['placeholder' => 'Ejem. 7777777777'],
            ])
            ->add('cedula', null, [
                'label'=>'Cedula Prof.',
                'attr' => ['placeholder' => 'Ejem. 12345678'],
            ])
            ->add('turno')
            ->add('trabajadorTipo', null, [
                'label'=>'Categoría'
            ])
            ->add('perfil')            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Trabajador::class,
        ]);
    }
}
