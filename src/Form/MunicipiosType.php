<?php

namespace App\Form;

use App\Entity\Municipios;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MunicipiosType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('municipioPais')
            ->add('nombreMunicipio')
            ->add('siglas')
            ->add('jurisdiccion')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Municipios::class,
        ]);
    }
}
