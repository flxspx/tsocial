<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        $entity = new User();
        $entity->setEmail('admin@gmail.com');
        $entity->setNombre('Administrador');
        $entity->setApellidos('Administrador');
        $password = $this->passwordHasher->hashPassword(
            $entity,
            'xswedcxs'
        );

        $entity->setPassword($password);
        $entity->setRoles(['ROL_ADMIN', 'ROLE_BACKEND']);
        // $product = new Product();
        // $manager->persist($product);
        $manager->persist($entity);
        $manager->flush();
    }
}
