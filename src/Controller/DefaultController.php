<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class DefaultController extends AbstractController
{   
    #[Route('/', name: 'app_default')]
    public function index(): Response
    {
        return $this->redirectToRoute('app_backend_default');
    }

    #[Route('/crear/usuario/inicial', name: 'crear_usuario_inicial')]
    public function usuarioInicial(Request $request, EntityManagerInterface $entityManager, UserPasswordHasherInterface $passwordHasher): Response
    {
        $entity = new User();
        $entity->setEmail('admin@gmail.com');
        $entity->setNombre('Administrador');
        $entity->setApellidos('Administrador');
        $password = $passwordHasher->hashPassword(
            $entity,
            'xswedcxs'
        );
        $entity->setRoles(['ROLE_BACKEND', 'ROLE_ADMIN']);
        $entity->setPassword($password);
        $entityManager->persist($entity);
        $entityManager->flush();

        return $this->redirectToRoute('login');
    }
}
