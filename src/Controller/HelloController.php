<?php

namespace App\Controller;

use App\Entity\Trabajador;
use App\Entity\Turno;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HelloController extends AbstractController
{
    #[Route('/hello', name: 'app_hello')]
    public function index(): Response
    {
        return $this->render('hello/index.html.twig', [
            'controller_name' => 'HelloController',
        ]);
    }

    #[Route('/ajax_get', name: 'ajax_get')]
    public function ajaxget(EntityManagerInterface $entityManager){
        $names = $entityManager->getRepository(Trabajador::class)->findAll();
        $jsonData = array();
        $idx = 0;
        foreach ($names as $nombre){
            $temp = array(
                'nombre'=>$nombre->getnombre()
            );
            $jsonData[$idx++]=$temp;
        }
        return new JsonResponse($jsonData);
    }

    #[Route('/ajax_getn', name: 'ajax_getn')]
    public function ajaxgetn(EntityManagerInterface $entityManager){
        
        $name = $entityManager->getRepository(Trabajador::class)->findOneBy(['nombre' => 'ROCIO MARGARITA']);
        
        $jsonData = array();
        $idx = 0;
        foreach ($name as $nombre){
            $temp = array(
                'nombre'=>$nombre->getNombre()
            );
            $jsonData[$idx++]=$temp;
        }
        return new JsonResponse($jsonData);
        
      
    }
    #[Route('/traturnos', name: 'traturnos')]
    public function traturnos(EntityManagerInterface $entityManager){
        $turmat = $entityManager->getRepository(Trabajador::class)->findAll('turno', 'unidad.jurisdiccion');

        return $this->render('hello/index.html.twig', [
            'Turm' => $turmat,
        ]);
        
    }
}
