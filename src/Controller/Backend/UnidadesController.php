<?php

namespace App\Controller\Backend;

use App\Entity\Unidades;
use App\Form\UnidadesType;
use App\Repository\UnidadesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/backend/unidades')]
class UnidadesController extends AbstractController
{
    #[Route('/', name: 'app_backend_unidades_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $unidades = $entityManager
            ->getRepository(Unidades::class)
            ->findAll();

        return $this->render('backend/unidades/index.html.twig', [
            'unidades' => $unidades,
        ]);
    }

    #[Route('/new', name: 'app_backend_unidades_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $unidade = new Unidades();
        $form = $this->createForm(UnidadesType::class, $unidade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($unidade);
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_unidades_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/unidades/new.html.twig', [
            'unidade' => $unidade,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_unidades_show', methods: ['GET'])]
    public function show(Unidades $unidade): Response
    {
        return $this->render('backend/unidades/show.html.twig', [
            'unidade' => $unidade,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_backend_unidades_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Unidades $unidade, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(UnidadesType::class, $unidade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_unidades_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/unidades/edit.html.twig', [
            'unidade' => $unidade,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_unidades_delete', methods: ['POST'])]
    public function delete(Request $request, Unidades $unidade, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$unidade->getId(), $request->request->get('_token'))) {
            $entityManager->remove($unidade);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_backend_unidades_index', [], Response::HTTP_SEE_OTHER);
    }
}
