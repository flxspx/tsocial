<?php

namespace App\Controller\Backend;

use App\Entity\Trabajador;
use App\Form\TrabajadorType;
use App\Repository\TrabajadorRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


#[Route('/backend/trabajador')]
class TrabajadorController extends AbstractController
{   
    #[Route('/', name: 'app_backend_trabajador_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $trabajadors = $entityManager
            ->getRepository(Trabajador::class)
            ->findAll();

        return $this->render('backend/trabajador/index.html.twig', [
            'trabajadors' => $trabajadors,
        ]);
    }

    #[Route('/new', name: 'app_backend_trabajador_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        if(!$this->isGranted('ROLE_TRABAJADOR') && !$this->isGranted('ROLE_ADMIN')){
            throw new AccessDeniedException('Se Necesitan Permisos Adicionales para poder realizar un nuevo registro');
        }
        
        $trabajador = new Trabajador();
        $form = $this->createForm(TrabajadorType::class, $trabajador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($trabajador);
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_trabajador_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/trabajador/new.html.twig', [
            'trabajador' => $trabajador,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_trabajador_show', methods: ['GET'])]
    public function show(Trabajador $trabajador): Response
    {
        return $this->render('backend/trabajador/show.html.twig', [
            'trabajador' => $trabajador,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_backend_trabajador_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Trabajador $trabajador, EntityManagerInterface $entityManager): Response
    {
        if(!$this->isGranted('ROLE_TRABAJADOR')){
            throw new AccessDeniedException('Se Necesitan Permisos Adicionales para poder modificar un registro');
        }

        $form = $this->createForm(TrabajadorType::class, $trabajador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_trabajador_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/trabajador/edit.html.twig', [
            'trabajador' => $trabajador,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_trabajador_delete', methods: ['POST'])]
    public function delete(Request $request, Trabajador $trabajador, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$trabajador->getId(), $request->request->get('_token'))) {
            $entityManager->remove($trabajador);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_backend_trabajador_index', [], Response::HTTP_SEE_OTHER);
    }





}

