<?php

namespace App\Controller\Backend;

use App\Entity\Turno;
use App\Form\TurnoType;
use App\Repository\TurnoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/backend/turno')]
class TurnoController extends AbstractController
{
    #[Route('/', name: 'app_backend_turno_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $turnos = $entityManager
            ->getRepository(Turno::class)
            ->findAll();

        return $this->render('backend/turno/index.html.twig', [
            'turnos' => $turnos,
        ]);
    }

    #[Route('/new', name: 'app_backend_turno_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $turno = new Turno();
        $form = $this->createForm(TurnoType::class, $turno);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($turno);
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_turno_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/turno/new.html.twig', [
            'turno' => $turno,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_turno_show', methods: ['GET'])]
    public function show(Turno $turno): Response
    {
        return $this->render('backend/turno/show.html.twig', [
            'turno' => $turno,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_backend_turno_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Turno $turno, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TurnoType::class, $turno);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_turno_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/turno/edit.html.twig', [
            'turno' => $turno,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_turno_delete', methods: ['POST'])]
    public function delete(Request $request, Turno $turno, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$turno->getId(), $request->request->get('_token'))) {
            $entityManager->remove($turno);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_backend_turno_index', [], Response::HTTP_SEE_OTHER);
    }


#[Route('/juris/js', name: 'app_backend_trabajador_juris', methods: ['GET'])]
    public function juris(EntityManagerInterface $entityManager): Response
    {
         
        $conn = $entityManager->getConnection();
        
        $sql = "select tbr.turno_id, uds.JurisdiccionId, trn.nombre_turno, js.Nombre, count(tbr.id) as cantidad FROM trabajador as tbr inner join Unidades as uds on tbr.unidad_id = uds.UnidadId inner join turno as trn on tbr.turno_id = trn.id inner join Jurisdicciones as js on uds.JurisdiccionId = js.JurisdiccionId group by tbr.turno_id, uds.JurisdiccionId,trn.nombre_turno,js.Nombre order by tbr.turno_id, trn.nombre_turno, uds.JurisdiccionId, js.Nombre, cantidad";
        
        $resuls = $conn->executeQuery($sql)->fetchAllAssociative();
        return $this->render('backend/turno/juris.html.twig', [
            'resuls' => $resuls,
        ]);
    }
}