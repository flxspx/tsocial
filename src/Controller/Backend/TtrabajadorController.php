<?php

namespace App\Controller\Backend;

use App\Entity\Ttrabajador;
use App\Form\TtrabajadorType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\TtrabajadorRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/backend/ttrabajador')]
class TtrabajadorController extends AbstractController
{
    #[Route('/', name: 'app_backend_ttrabajador_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $ttrabajadors = $entityManager
            ->getRepository(Ttrabajador::class)
            ->findAll();

        return $this->render('backend/ttrabajador/index.html.twig', [
            'ttrabajadors' => $ttrabajadors,
        ]);
    }

    #[Route('/new', name: 'app_backend_ttrabajador_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $ttrabajador = new Ttrabajador();
        $form = $this->createForm(TtrabajadorType::class, $ttrabajador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($ttrabajador);
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_ttrabajador_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/ttrabajador/new.html.twig', [
            'ttrabajador' => $ttrabajador,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_ttrabajador_show', methods: ['GET'])]
    public function show(Ttrabajador $ttrabajador): Response
    {
        return $this->render('backend/ttrabajador/show.html.twig', [
            'ttrabajador' => $ttrabajador,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_backend_ttrabajador_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Ttrabajador $ttrabajador, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TtrabajadorType::class, $ttrabajador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_ttrabajador_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/ttrabajador/edit.html.twig', [
            'ttrabajador' => $ttrabajador,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_ttrabajador_delete', methods: ['POST'])]
    public function delete(Request $request, Ttrabajador $ttrabajador, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ttrabajador->getId(), $request->request->get('_token'))) {
            $entityManager->remove($ttrabajador);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_backend_ttrabajador_index', [], Response::HTTP_SEE_OTHER);
    }
}
