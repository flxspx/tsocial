<?php

namespace App\Controller\Backend;

use App\Entity\Municipios;
use App\Form\MunicipiosType;
use App\Repository\MunicipiosRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/backend/municipios')]
class MunicipiosController extends AbstractController
{
    #[Route('/', name: 'app_backend_municipios_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $municipios = $entityManager
            ->getRepository(Municipios::class)
            ->findAll();

        return $this->render('backend/municipios/index.html.twig', [
            'municipios' => $municipios,
        ]);
    }

    #[Route('/new', name: 'app_backend_municipios_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $municipio = new Municipios();
        $form = $this->createForm(MunicipiosType::class, $municipio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($municipio);
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_municipios_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/municipios/new.html.twig', [
            'municipio' => $municipio,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_municipios_show', methods: ['GET'])]
    public function show(Municipios $municipio): Response
    {
        return $this->render('backend/municipios/show.html.twig', [
            'municipio' => $municipio,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_backend_municipios_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Municipios $municipio, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(MunicipiosType::class, $municipio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_municipios_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/municipios/edit.html.twig', [
            'municipio' => $municipio,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_municipios_delete', methods: ['POST'])]
    public function delete(Request $request, Municipios $municipio, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$municipio->getId(), $request->request->get('_token'))) {
            $entityManager->remove($municipio);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_backend_municipios_index', [], Response::HTTP_SEE_OTHER);
    }
}
