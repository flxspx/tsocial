<?php

namespace App\Controller\Backend;

use App\Entity\Jurisdicciones;
use App\Form\JurisdiccionesType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\JurisdiccionesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/backend/jurisdicciones')]
class JurisdiccionesController extends AbstractController
{
    #[Route('/', name: 'app_backend_jurisdicciones_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $jurisdicciones = $entityManager
            ->getRepository(Jurisdicciones::class)
            ->findAll();

        return $this->render('backend/jurisdicciones/index.html.twig', [
            'jurisdicciones' => $jurisdicciones,
        ]);
    }

    #[Route('/new', name: 'app_backend_jurisdicciones_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $jurisdiccione = new Jurisdicciones();
        $form = $this->createForm(JurisdiccionesType::class, $jurisdiccione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($jurisdiccione);
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_jurisdicciones_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/jurisdicciones/new.html.twig', [
            'jurisdiccione' => $jurisdiccione,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_jurisdicciones_show', methods: ['GET'])]
    public function show(Jurisdicciones $jurisdiccione): Response
    {
        return $this->render('backend/jurisdicciones/show.html.twig', [
            'jurisdiccione' => $jurisdiccione,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_backend_jurisdicciones_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Jurisdicciones $jurisdiccione, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(JurisdiccionesType::class, $jurisdiccione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_backend_jurisdicciones_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('backend/jurisdicciones/edit.html.twig', [
            'jurisdiccione' => $jurisdiccione,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_backend_jurisdicciones_delete', methods: ['POST'])]
    public function delete(Request $request, Jurisdicciones $jurisdiccione, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$jurisdiccione->getId(), $request->request->get('_token'))) {
            $entityManager->remove($jurisdiccione);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_backend_jurisdicciones_index', [], Response::HTTP_SEE_OTHER);
    }
}