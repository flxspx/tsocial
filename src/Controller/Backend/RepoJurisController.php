<?php

namespace App\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RepoJurisController extends AbstractController
{
    #[Route('/backend/repo/juris', name: 'app_backend_repo_juris')]
    public function index(): Response
    {
        return $this->render('backend/repo_juris/index.html.twig', [
            'controller_name' => 'RepoJurisController',
        ]);
    }
}